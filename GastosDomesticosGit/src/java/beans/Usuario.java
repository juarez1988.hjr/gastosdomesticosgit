package beans;

import java.sql.*;
import java.util.*;

public class Usuario {

    private int idUsuario;
    private String usuario;
    private String password;
    private String nombre;
    private String apellido1;
    private String apellido2;
    private String dni;
    private String perfil;
    private int eliminado;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getEliminado() {
        return eliminado;
    }

    public void setEliminado(int eliminado) {
        this.eliminado = eliminado;
    }
     
    public List<Usuario> getLoguin(String usuario, String password){
        List<Usuario> usuarios = new ArrayList<Usuario>();
        try{
            //Conectamos y realizamos la consulta
            Connection conn = Conexion.getConnection();
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM usuario WHERE usuario = '"+usuario+"' AND password = MD5('"+password+"') AND eliminado = 0 LIMIT 0,1");
            ResultSet rs = stmt.executeQuery();
            //Recorremos los resultados y los vamos metiendo para la lista
            while(rs.next()){
                int idUsuarioSQL = rs.getInt("idUsuario");
                String usuarioSQL = rs.getString("usuario");
                String passwordSQL = rs.getString("password");
                String nombreSQL = rs.getString("nombre");
                String apellido1SQL = rs.getString("apellido1");
                String apellido2SQL = rs.getString("apellido2");
                String dniSQL = rs.getString("dni");
                String perfilSQL = rs.getString("perfil");
                int eliminadoSQL = rs.getInt("eliminado");
                
                Usuario usuarioActual = new Usuario();
                usuarioActual.setIdUsuario(idUsuarioSQL);
                usuarioActual.setUsuario(usuarioSQL);
                usuarioActual.setPassword(passwordSQL);
                usuarioActual.setNombre(nombreSQL);
                usuarioActual.setApellido1(apellido1SQL);
                usuarioActual.setApellido2(apellido2SQL);
                usuarioActual.setDni(dniSQL);
                usuarioActual.setPerfil(perfilSQL);
                usuarioActual.setEliminado(eliminadoSQL);
                
                usuarios.add(usuarioActual);
                   
            }
            Conexion.close(rs);
            Conexion.close(stmt);
            Conexion.close(conn);
        }catch(Exception e){
            e.printStackTrace();
        }
        return usuarios;
    } 
}
