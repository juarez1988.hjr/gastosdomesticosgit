<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%  
if(!session.getAttribute("perfilSession").equals("administrador") && !session.getAttribute("perfilSession").equals("estandar")){
    //Redireccionamos a logarse, index.jsp
    response.sendRedirect("index.jsp?tipoAviso=alert alert-danger&aviso=Datos incorrectos, comprueba los datos de logueo.");
}
%>