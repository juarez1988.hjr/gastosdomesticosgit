<%-- 
    Document   : header
    Created on : 06-feb-2019, 18:39:01
    Author     : PRUEBA
--%>   
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="./images/icons/icono.ico" />

    <title>Gastos Dom&eacute;sticos</title>
    <!-- jquery -->
    <script src="./js/vendor/jquery-1.11.2.min.js"></script>
    <script src="./js/vendor/bootstrap.min.js"></script>
    <script src="./js/vendor/moment-with-locales.js"></script>
    <script src="./js/vendor/bootstrap-datetimepicker.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
	<!-- Bootstrap calendario css-->
    <link href="./css/bootstrap-datetimepicker.css" rel="stylesheet" media="screen">
	

    </head>
  <body>
  <input type="hidden" id="antesHead">
  <!-- inicio container -->
  <div class="container">
    
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		  <!-- El logotipo y el icono que despliega el menú se agrupan
			   para mostrarlos mejor en los dispositivos móviles -->
		  <div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			  <span class="sr-only">Desplegar navegación</span>
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">
			<div id="general_logo logo">
				<img class="apDiv1 sobre" src="./images/logo-min.jpg"/>  
			</div>
			</a>
		  </div>
		  
		  <!-- Agrupar los enlaces de navegación, los formularios y cualquier
			   otro elemento que se pueda ocultar al minimizar la barra -->
		  <div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav" id="menu_superior">
				<li class=" ">
					<a href="./bienvenida.jsp"><span class="glyphicon glyphicon-home"></span> Inicio</a>
				</li>
				<li class=" ">
					<a href="./insertarMovimientos.jsp"><span class="glyphicon glyphicon-stats"></span> Inser. Movimientos</a>
				</li>
				<li class=" ">
					<a href="./listarMovimientos.jsp"><span class="glyphicon glyphicon-eur"></span> Listar Movimientos</a>
				</li>
				
                                <%
				//if(session.getAttribute("perfilSession").equals("administrador")){
                                %>
                                <c:if test="${sessionScope.perfilSession=='administrador'}">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						  <span class="glyphicon glyphicon-cog"></span> Gesti&oacute;n <b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<li><a href="./gestionUsuarios.jsp"><span class="glyphicon glyphicon-hdd"></span> Usuarios</a></li>
							<li><a href="./gestionCategorias.jsp"><span class="glyphicon glyphicon-stats"></span> Categorías</a></li>
						</ul>
					</li>
                                </c:if>
				<%	
				//}
                                %>
				<li class=" "><a href="SalirController" class="navbar-link"><span class="glyphicon glyphicon-log-out"></span> Salir</a></li>
			</ul>
			<p class="navbar-text pull-right">
			  Conectado como 
                          <a href="cerrarSesion.php" class="navbar-link">
                              <!--<%=session.getAttribute("usuarioSession")%>-->
                              <c:out value="${sessionScope.usuarioSession}"/>
                          </a>
			</p>
		  </div>
	</nav>
<%
String tipoAviso = request.getParameter("tipoAviso");
String aviso = request.getParameter("aviso");
if(aviso==null && tipoAviso==null){
    tipoAviso = "";
    aviso = "";
}
%>