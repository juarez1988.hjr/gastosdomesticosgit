<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
String tipoAviso = request.getParameter("tipoAviso");
String aviso = request.getParameter("aviso");
if(aviso==null && tipoAviso==null){
    tipoAviso = "";
    aviso = "";
}
%>

<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="shortcut icon" href="./images/icons/icono.ico" />
    <title>Gastos Domésticos</title>
    <!-- jquery -->
    <script src="js/vendor/jquery-1.11.2.min.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Loading CSS -->
    <link href="css/propio/loading.css" rel="stylesheet">
    <!-- css pantalla login -->
    <link href="css/propio/login.css" rel="stylesheet">
  </head>
  <body>
    <div class="mascara">
	<div class="preloader">
            <div class="load1"></div>
            <div class="load2"></div>
	</div>
    </div>
    <div class="container">
	<br>
        <div class="alert <%=tipoAviso%>"><%=aviso%>&nbsp;</div>
	  <center><h2 class="form-signin-heading"><img width="25%" src="./images/logo.png"/></h2></center>
          <br>
      <form class="form-horizontal" action="UsuarioController" method="post" name="form_login">
		<div class="form-group form-group-lg">
			<div class="col-sm-offset-4 col-sm-4">
				<input class="form-control" name="accion" type="hidden" id="accion" value="logueo">
				<input class="form-control" name="usuario" type="text" id="usuario" placeholder="Usuario" required autofocus>
			</div>
		</div>
		<div class="form-group form-group-lg">
			<div class="col-sm-offset-4 col-sm-4">
				<input name="password" type="password" id="password" class="form-control" placeholder="Password" required>
			</div>
		</div>
		<div class="form-group form-group-lg">
			<div class="col-sm-offset-4 col-sm-4">
				<button class="btn btn-lg btn-primary btn-block" type="submit" id="Entrar">Entrar</button>
			</div>
		</div>
      </form>
    </div> 
  </body>
</html>