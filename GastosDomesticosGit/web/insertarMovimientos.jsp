<%@include file="comprobarConexion.jsp"%>
<%@include file="header.jsp"%>
        
        <div class="jumbotron text-center">
            <h1>Insertar Movimientos</h1>
            <h2>Inserta aqu� tus movimientos</h2>
	</div>
        
        <div class="form-group form-group-lg">
            <center>
            <div class="alert <%=tipoAviso%>"><%=aviso%>&nbsp;</div>
            
            
            
            
            <div id="crearObjetivo">			
		<br/>					
		<form class="form-horizontal" action="MovimientoController" method="post">
                    <input class="form-control" name="accion" type="hidden" id="accion" value="insertarMovimiento">
                    <div class="form-group form-group-lg">
			<div class="col-sm-offset-4 col-sm-5">
                            <label for="idGrupoFK">Grupo objetivo al que aplica: </label>
                            <select class="form-control" name="idGrupoFK" id="idGrupoFK" onChange="buscarUsuariosVicidial(this.value)" required>
				<option value="">Selecciona grupo-campa&ntilde;a-servidor</option>
                            </select>
			</div>
                    </div>
						
                    <div class="form-group form-group-lg">
                        <div class="col-sm-offset-4 col-sm-5">
                            <label for="usuarioVicidial">Usuario Vicidial: </label>
                            <select class="form-control" name="usuarioVicidial" id="usuarioVicidial" required>
                            </select>
                        </div>
                    </div>
						
                    <div class="form-group form-group-lg">
                        <div class="col-sm-offset-4 col-sm-4">
                            <label for="fecha">Fecha ventas: </label>
                            <input class="form-control" name="fecha" type="text" id="fecha" placeholder="AAAA-MM-DD" required>
                        </div>
                    </div>
						
                    <div class="form-group form-group-lg">
                        <div class="col-sm-offset-4 col-sm-4">
                            <label for="nVentasBrutas">N&uacute;mero de ventas brutas: </label>
                            <input class="form-control" name="nVentasBrutas" type="number" id="nVentasBrutas" placeholder="N&uacute;mero de ventas brutas" required>
                        </div>
                    </div>
						
                    <div class="form-group form-group-lg">
                        <div class="col-sm-offset-4 col-sm-4">
                            <label for="importeTotalVentas">Importe total ventas: </label>
                            <input class="form-control" name="importeTotalVentas" type="number" id="importeTotalVentas" placeholder="Importe total ventas" required>
                        </div>
                    </div>
						
						
                    <div class="form-group form-group-lg">
                        <div class="col-sm-offset-4 col-sm-4">
                            <button class="btn btn-lg btn-primary btn-block" type="submit" id="Entrar">Insertar ventas</button>
                        </div>
                    </div>					
                </form>
            </div>
            </center>
	</div>
<%@include file="footer.jsp"%>
